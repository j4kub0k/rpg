extends CanvasLayer

var  scene
var  fading = false
# Called when the node enters the scene tree for the first time.
func fade_in(next_scene):
	scene=next_scene
	$AnimationPlayer.play("Fade")
	fading =true
	
func fade_out():
	$AnimationPlayer.play_backwards("Fade")
	fading=false

func _on_animation_player_animation_finished(anim_name):
	if fading:
		get_tree().change_scene_to_file(scene)
		fade_out()
