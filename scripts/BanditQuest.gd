extends Node2D

# Called when the node enters the scene tree for the first time.
var complete = false
var run=false
# Called when the node enters the scene tree for the first time.

func _set_process(a:bool):
	run=true
func _ready():
	$Label.text= 'get rid of bandit'
	
	if PlayerState.quests.is_empty() and PlayerState.quest_enabled!=2:
		queue_free()
		return

func Complete():
	$Label.text='Quest Completed'
	$Timer.start()
	await $Timer.timeout
	queue_free()

func _process(delta):
	if not complete and run:
		if World.bandints.is_empty():
			Complete()
			PlayerState.quest_enabled=0
			complete=true

