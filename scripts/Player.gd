class_name Player
extends Area2D


@export var speed: float = 4
@export var damage:int = 10
signal moved
var movement_tween:Tween


const TILE_SIZE = 16

const inputs = {
	"Left": Vector2.LEFT,
	"Right": Vector2.RIGHT,
	"Up": Vector2.UP,
	"Down": Vector2.DOWN
}

var last_dir = Vector2.ZERO
var last_action = ""

func _get_quest(quest):
	match quest:
		1:
			$CoinQuest.visible=true
			$CoinQuest._set_process(true)
		2:
			$BanditQuest.visible=true
			if get_parent().name=='World':
				$BanditQuest._set_process(true)
func _ready():
	_get_quest(PlayerState.quest_enabled)
	if PlayerState.position_set:
		print(PlayerState.position, 'setting')
		position=PlayerState.position
	position.x = int(position.x / TILE_SIZE) * TILE_SIZE
	position.y = int(position.y / TILE_SIZE) * TILE_SIZE
	position += Vector2.ONE * TILE_SIZE/2
	# set timer interval according to the speed
	$MoveTimer.wait_time = 1.0/speed

func _unhandled_input(event):
	for action in inputs:
		if event.is_action_pressed(action):
			var dir = inputs[action]
			if move_tile(dir):
				# repeat the action in fixed intervals, if it is still pressed
				last_action = action
				last_dir = dir
				$MoveTimer.start()

func move_tile(direction: Vector2):
	if movement_tween != null and movement_tween.is_running():
		return
	$RayCast2D.target_position = direction * TILE_SIZE
	$RayCast2D.force_raycast_update()
	if !$RayCast2D.is_colliding():
		var newpos = position + direction * TILE_SIZE
		movement_tween=create_tween()
		movement_tween.tween_property(self,"position",newpos,1/speed).set_trans(Tween.TRANS_CUBIC).set_ease(Tween.EASE_IN_OUT)
		##moved.emit()
		return true
	var other = $RayCast2D.get_collider()
	print(other)
	if(other is Collidable):
		other.on_collision(self)
	return false

func _on_MoveTimer_timeout():
	if Input.is_action_pressed(last_action):
		if move_tile(last_dir):  # do the same move as the last time
			return
	# reset
	last_action = ""
	last_dir = Vector2.ZERO
	$MoveTimer.stop()

func get_hurt(value):
	get_viewport().get_camera_2d().shake()
	PlayerState.decrease_health(value)
	
	var floating_text = preload("res://scenes/FloatingText.tscn") 
	var instance=floating_text.instantiate()
	instance.position=self.position+Vector2.UP*TILE_SIZE
	get_parent().add_child(instance)
	instance.start_floating_animation(value)
	
	$Sprite2D.modulate=Color.RED
	$GetHurtTimer.start()
	await $GetHurtTimer.timeout
	$Sprite2D.modulate=Color.WHITE
	
	

func collect_coins(value):
	PlayerState.add_coins(value)


