extends Area2D

@export var lives = 20
var is_active=false
var message:String = 'Give me all your \n money press space'

func _ready():
	$Label.text = message
	$Label.hide()

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if PlayerState.coins > 0:
			PlayerState.add_coins(-PlayerState.coins)
			queue_free()
			get_parent().bandints.clear()
		else:
			$Label.text='no coins \n than fight'
func get_hurt(value):
	$Label.hide()
	lives -= value
	var floating_text = preload("res://scenes/FloatingText.tscn") 
	var instance=floating_text.instantiate()
	instance.position=self.position+Vector2.UP*Player.TILE_SIZE
	get_parent().add_child(instance)
	instance.start_floating_animation(value)
	
	$Sprite2D.modulate=Color.RED
	$GetHurtTimer.start()
	await $GetHurtTimer.timeout
	$Sprite2D.modulate=Color.WHITE
	if(lives <= 0):
		get_parent().bandints.clear()
		queue_free()


func _on_area_entered(area):
	$Label.show()
	is_active=true


func _on_area_exited(area):
	$Label.hide()
	is_active=false
	$Label.text=message
