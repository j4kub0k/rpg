extends Area2D

@export var amount := 0
@export var scale_curve: Curve

var _random_offset = randi()

func _process(delta):
	var time = Time.get_ticks_msec() + _random_offset
	var x = (time % 1000)/ 1000.0
	$Sprite2D.scale =Vector2.ONE*scale_curve.sample(x)


func _on_area_entered(area):
	if area is Player:
		area.collect_coins(amount)
		self.queue_free()
