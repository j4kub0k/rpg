class_name Collidable
extends CollisionObject2D

@export var damage:int =10
@export var damagable:bool
@export var lives:int = 20
# Called when the node enters the scene tree for the first time.
func on_collision(other: CollisionObject2D):
	if(other is Player):
		other.get_hurt(damage)
	print(damagable)
	if damagable:
		self.get_parent().get_hurt(other.damage)
		
		
