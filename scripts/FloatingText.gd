extends Label

var floating_tween: Tween = null

	
func start_floating_animation(value):
	# Set initial position
	if not value is  String:
		text=str(value)
	print(text)
	floating_tween=create_tween()
	floating_tween.set_parallel()
	floating_tween.tween_property(self,'position',position+Vector2.UP*10,2)
	floating_tween.tween_property(self,'modulate',Color.TRANSPARENT,2)
	floating_tween.tween_property(self,'font_size',0,2)
	$Timer.start()
	await $Timer.timeout
	queue_free()

