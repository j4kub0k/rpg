extends Node2D

@export var message: String = "hello"
var dialog = ['I have a quest for you', 'Do you want it' , 'press space']
var is_active=false
var player 
# Called when the node enters the scene tree for the first time.
func _ready():
	$Label.text=message
	$Label.hide()
	if(PlayerState.quests.is_empty()):
		dialog.clear()
	elif PlayerState.quests[0] == 2:
		dialog=['There is a bandit', 'Get rid of him']

func give_quest(number):
	if(PlayerState.quest_enabled==0):
		match number:
			1:
				player._get_quest(number)
				PlayerState.quests.remove_at(0)
				$Label.text='GO UP and LEFT'
				is_active=false
				PlayerState.quest_enabled=number
			2:
				player._get_quest(number)
				PlayerState.quest_enabled=PlayerState.quests[0]
				PlayerState.quests.remove_at(0)
				$Label.text='Go right from house'
				is_active=false
				dialog.clear()
			_:
				PlayerState.quest_enabled=0
	else:
		$Label.text = 'Uncomplite quest'

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if not PlayerState.quests.is_empty():
			give_quest(PlayerState.quests[0])

func _dialog():
	for i in dialog:
		if is_active and not dialog.is_empty():
			$Timer.start()
			await $Timer.timeout
			$Label.text = i
		else:
			return 

func _on_area_entered(area):
	if area is Player:
		$Label.show()
		is_active=true
		_dialog()
		player=area


func _on_area_exited(area):
	$Label.hide()
	is_active=false
	$Label.text=message
