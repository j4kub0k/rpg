extends Node2D

@export var amout := 20
var current
var complete = false
var run=false
# Called when the node enters the scene tree for the first time.

func _set_process(a:bool):
	run=true
	update_coins()
func _ready():
	$Label.text='collect: ' + str(amout)
	current = PlayerState.coins
	update_coins()
	if PlayerState.quests.is_empty():
		queue_free()
		return
	if PlayerState.quests[0]!=1 and  PlayerState.quest_enabled!=1 :
		queue_free()

func Complete():
	$Label.text='Quest Completed'
	$Timer.start()
	await $Timer.timeout
	queue_free()

func  update_coins():
	$Label.text = 'colletct: ' + str(amout-PlayerState.coins)
# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if not complete and run:
		if PlayerState.coins != current:
			update_coins()
		if PlayerState.coins >= amout:
			Complete()
			PlayerState.quest_enabled=0
			complete=true
