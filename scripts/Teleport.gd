extends Area2D


@export var next_scene: String = "res://scenes/House.tscn"
@export var message: String = "Press SPACE to enter."
@export var target_location:Vector2

var is_active = false


func _ready():
	$CanvasLayer/TextureRect/Label.text = message
	#$CanvasLayer/TextureRect.hide()

func _unhandled_input(event):
	if event.is_action_pressed("Action") and is_active:
		if(not PlayerState.position_set):
			PlayerState.position=self.position
			PlayerState.position_set=true
		#get_tree().change_scene_to_file(next_scene)
		SceneTransition.fade_in(next_scene)
	


func _on_Teleport_area_entered(area):
	if area.is_in_group("Player"):
		#$CanvasLayer/TextureRect.show()
		$CanvasLayer/TextureRect/AnimationPlayer.play("pop_out")
		print(not PlayerState.position_set)
		is_active = true


func _on_Teleport_area_exited(area):
	if area.is_in_group("Player"):
		#$CanvasLayer/TextureRect.hide()
		$CanvasLayer/TextureRect/AnimationPlayer.play_backwards("pop_out")
		is_active = false
