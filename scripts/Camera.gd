extends Camera2D

@export  var follow_target:Node2D
@export  var speed:float=2.0
@export var shake_duration:=0.5
@export var shake_scale :=15
@export var shake_times :=10
var rnd=RandomNumberGenerator.new()
# Called when the node enters the scene tree for the first time.

func _physics_process(delta):
	position=lerp(position,follow_target.position,speed*delta)

func shake():
	var tween=create_tween()
	for i in range(shake_times-1):
		var newpos=Vector2(randf_range(-shake_scale,shake_scale), randf_range(-shake_scale,shake_scale))
		tween.tween_property(self,"offset",newpos,shake_duration/shake_times)
	tween.tween_property(self,"offset", Vector2.ZERO,shake_duration/shake_times)
	
		
