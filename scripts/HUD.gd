extends Node

var tween:Tween
var coin_tween:Tween
var coins=0

func update_health(value):
	if tween != null && tween.is_running():
		tween.stop()
	tween=create_tween()
	tween.tween_property($HealthBar,'value',value, 1.0 )
	#$HealthBar.value=value
func set_label_text(value: int):
	$Coins.text = str(value)
	
func update_coins(amount: int):
	if coin_tween != null && coin_tween.is_running():
		coin_tween.stop()
	coin_tween = create_tween()
	coin_tween.tween_method(set_label_text, coins, amount, 1)
	coins = amount

